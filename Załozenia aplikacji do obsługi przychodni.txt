Rejestracja
	- najwazniejsze informacje o pacjencie
	- dostęp do kartoteki pacjenta
	- powiadomienia email/sms
	- rejestracja wizyt
	- kalendarz 
	- dodawanie "ikon" do wizyt

Wizyta
	- tworzenie recept
	- ponawianie recept/pojedynczych leków
	- dostep do bazy leków z poziomem refundacji
	- zwolnienia
	- skierowania na badania i zapis wyników

Po wizycie
	- raporty i sprawozdania
	- płatności i rozliczenia
	- statystyki

dostęp dla admina, lekarza i pacjenta

Moduły:
- pacjenci
- lekarze
- wizyty
- recepty
- baza danych (leków, świadzczen, chorób)

narzedzie do projektowania gui i zaprojektować (przejscia, menu, onclick events)